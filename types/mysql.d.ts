export const Types: any;
export function createConnection(config: any): any;
export function createPool(config: any): any;
export function createPoolCluster(config: any): any;
export function createQuery(sql: any, values: any, callback: any): any;
export function escape(value: any, stringifyObjects: any, timeZone: any): any;
export function escapeId(value: any, forbidQualified: any): any;
export function format(sql: any, values: any, stringifyObjects: any, timeZone: any): any;
export function raw(sql: any): any;
