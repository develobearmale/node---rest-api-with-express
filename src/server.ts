import express from "express";
import morgan from "morgan";
import mysql from "mysql";
import cors from "cors";
import userRoutes from "./routes/user.routes";

class Server {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config() {
        this.app.set('port', process.env.PORT || 8081)
        this.app.use(morgan("dev"));
        this.app.use(express.urlencoded({extended: false})); //This is need for forms request (Web).
        this.app.use(express.json());
        this.app.use(cors());
    }

    routes() {
        this.app.use(userRoutes);
    }

    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log("Server on port", this.app.get('port'));
        });
    }
}

const server = new Server();
server.start();