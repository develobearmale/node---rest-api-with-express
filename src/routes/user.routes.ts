import { Request, Response, Router } from "express";
import Connection from "../db/connection";
import User from "../models/user";

class userRoutes {
    router: Router;
    connection: any;

    constructor() {
        this.router = Router();
        this.routes();
    }

    async getUsers(req: Request, res: Response) {

        const querString = "SELECT * FROM users WHERE active = 1"

        await Connection.connectionMYSQL().query(querString, (err: any, rows: any, fields: any) => {
            if (err) {
                console.log("Failed to query for users" + err)
                res.sendStatus(500)
                res.end()
                return
            }

            res.json(rows)
        })

    }

    async getUser(req: Request, res: Response) {

        const userId = req.params.id
        const querString = "SELECT * FROM users where id = ?"
        await Connection.connectionMYSQL().query(querString, [userId], (err: any, rows: any, fields: any) => {
            if (err) {
                console.log("Failed to query for users" + err)
                res.sendStatus(500)
                res.end()
                return
            }

            /*
            const users = rows.map((row)=>{
              return {Username: row.username}
            })*/

            res.json(rows)
        })


    }

    async createUser(req: Request, res: Response) {

        const querString = "INSERT INTO users(username,email,password,created_at,updated_at) values (?,?,?,?,?)";

        let objUser = new User(undefined, req.body.username, req.body.email, req.body.password, req.body.created_at, req.body.updated_at);

        await Connection.connectionMYSQL().query(querString, [objUser.username, objUser.email, objUser.password, objUser.created_at, objUser.updated_at], (err: any, results: any, fields: any) => {
            if (err) {
                console.log("Failed to insert users" + err)
                res.sendStatus(500)
                return
            }

            console.log("Inserted a new user: " + objUser);

        })

        res.end()

    }

    async updateUser(req: Request, res: Response) {

        const querString = "UPDATE users SET email = ?, password= ?, updated_at = now() WHERE id = ?";

        let objUser = new User(req.body.id, req.body.username, req.body.email, req.body.password, req.body.created_at, req.body.updated_at);

        await Connection.connectionMYSQL().query(querString, [objUser.email, objUser.password, objUser.id], (err: any, results: any, fields: any) => {
            if (err) {
                console.log("Failed to insert users" + err)
                res.sendStatus(500)
                return
            }

            console.log("Inserted a new user: " + objUser);

        })

        res.end()

    }

    async removeUser(req: Request, res: Response) {

        const userId = req.params.id

        const querString = "UPDATE users set active = '0' where id = ? "

        await Connection.connectionMYSQL().query(querString, [userId], (err: any, rows: any, fields: any) => {
            if (err) {
                console.log("Failed to query for users" + err)
                res.sendStatus(500)
                res.end()
                return
            }

            res.json(rows)
        })


    }

    routes() {
        this.router.get("/user/getall", this.getUsers);
        this.router.get("/user/get/:id", this.getUser);
        this.router.get("/user/delete/:id", this.removeUser);
        this.router.post("/user/create", this.createUser);
        this.router.post("/user/update", this.updateUser);
    }


}

const userRoute = new userRoutes();

export default userRoute.router;