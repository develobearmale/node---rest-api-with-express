
export default class user{
     public id: number ;
     public username : string;
     public email: string;
     public password : string;
     public created_at: Date;
     public updated_at: Date;
     public active: boolean;

     constructor(
        id?: number,
        username?: string,
        email?: string,
        password?: string,
        created_at?: Date,
        updated_at?: Date,
        active?: boolean,
     ){
         this.id = id || 0;
         this.username = username || " ";
         this.email = email || " ";
         this.password = password || " ";
         this.created_at = created_at || new Date;
         this.updated_at = updated_at || new Date;
         this.active = active || false;
     }
}